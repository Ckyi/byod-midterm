package com.kmc.kyimoecho.byod_midterm;

import android.util.Log;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashMap;

/**
 * Created by kyimoecho on 16-01-23.
 */
public class Exam {
    private static final String question_tag = "question";
    private static final String question_text_tag = "question_text";
    private static final String choice_tag = "choice";
    private static final String submission_email_tag = "submission_email_address";
    private static       String submission_emailAddress = "";
    private static final String id = "id";

    public static String getSubmission_emailAddress()
    {
        return submission_emailAddress;
    }

    public static ArrayList<Question> parseFrom(BufferedReader reader){
        boolean isQuestion   = false;
        boolean isChoice     = false;
        boolean isSubmissionEmail = false;
        Integer index        = 0;
        Integer questionId   = 0;
        String  choiceId     = "";
        String  questionText = "";
        String  tag;
        ArrayList<Question>    questions  = new ArrayList<>(); // = Question.exampleSet1(); //for now
        HashMap<String,String> choiceList = new HashMap<>();


        // Get our factory and create a PullParser
        XmlPullParserFactory factory = null;
        try {
            factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();

            xpp.setInput(reader); // set input file for parser
            int eventType = xpp.getEventType(); // get initial eventType

            // Loop through pull events until we reach END_DOCUMENT
            while (eventType != XmlPullParser.END_DOCUMENT) {

                // handle the xml tags encountered
                switch (eventType) {
                    case XmlPullParser.START_TAG: //XML opening tags
                     /*   //to do
                        Log.i(TAG, "START_TAG: " + xpp.getName());

                        if(xpp.getAttributeCount() > 0) {
                            Log.i(TAG, "ATTRIBUTE_NAME: " + xpp.getAttributeName(0));
                            Log.i(TAG, "ATTRIBUTE_VALUE: " + xpp.getAttributeValue(0));
                            contrib = xpp.getAttributeValue(0);
                        }
*/
                        tag = xpp.getName();
                        if(tag.equals(question_tag)){
                            questionId = Integer.parseInt(xpp.getAttributeValue(0));
                        }
                        else if(tag.equals(question_text_tag)) {
                            isQuestion = true;
                        }
                        else if(tag.equals(choice_tag)) {
                            isChoice = true;
                            choiceId = xpp.getAttributeValue(null,id);
                        }
                        else if(tag.equals(submission_email_tag))
                            isSubmissionEmail = true;

                        break;

                    case XmlPullParser.TEXT:
                        //to do
                        //Log.i(TAG, "TEXT: " + xpp.getText().trim());
                        if(isQuestion) {
                            questionText += xpp.getText().trim();
                        }
                        else if(isChoice)
                        {
                            String choice = xpp.getText();
                            choiceList.put(choiceId,choice);
                            index++;
                        }
                        else if(isSubmissionEmail)
                        {
                            submission_emailAddress = xpp.getText().trim();
                            isSubmissionEmail = false;
                        }

                        break;

                    case XmlPullParser.END_TAG: //XML closing tags
                        //to do
                        //Log.i(TAG, "END_TAG: " + xpp.getName());
                        /*if(xpp.getName().equals("question"))
                            isQuestion = false;

                        if(xpp.getName().equals("answer"))
                            isAnswer = false;*/
                        if(xpp.getName().equals(question_text_tag)) {
                            isQuestion = false;
                        }

                        if(xpp.getName().equals(choice_tag)) {
                            isChoice = false;

                            if(index > 4) {
                                questions.add(new Question(questionText,choiceList,questionId));
                                index = 0;
                                questionText = "";
                                choiceList = new HashMap<>();
                            }
                        }
                        break;

                    default:
                        break;
                }
                //iterate
                eventType = xpp.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (java.io.IOException e) {
            e.printStackTrace();
        }



        //return questions;
        return questions;

    }


}

