package com.kmc.kyimoecho.byod_midterm;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by kyimoecho on 16-01-25.
 */
public class States {
    static final String STATE_QUESTION = "question";
    static final String STATE_ANSWER = "answer";
    private static ArrayList<Question> questions = null;
    private static ArrayList<String> answers = new ArrayList<>();

    public static void initializeAnswers(int size) {
        for(int i=0; i<size; i++){
            answers.add(i,"");
        }
    }

    public static ArrayList<Question> getQuestions()
    {
        return questions;
    }

    public static void setQuestions(ArrayList<Question> argQuestions)
    {
        questions = argQuestions;
    }

    public static ArrayList<String> getAnswers() { return answers; }

    public static void setAnswers(ArrayList<String> argAnswers) { answers = argAnswers; }
}
