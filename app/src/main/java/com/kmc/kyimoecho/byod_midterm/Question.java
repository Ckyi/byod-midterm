package com.kmc.kyimoecho.byod_midterm;



import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by kyimoecho on 16-01-23.
 */
public class Question {

    /* Represents multiple choice question, including its answer
    */

    private  final String TAG = this.getClass().getSimpleName();

    //XML tags of Question object

    private String questionString; //question text
    private HashMap<String,String> choiceList; //boolean representing the question answer
    private Integer id;

    public Question(String argQuestionString, HashMap<String,String> argChoiceList, Integer argId){
        questionString = argQuestionString;
        choiceList = argChoiceList;
        id = argId;
    }

    public Integer getId(){return id;}
    public String getQuestionString(){return questionString;}
    public HashMap<String,String> getChoiceList(){return choiceList;}

}
